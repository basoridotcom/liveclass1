# LIVE CLASS STREAMING MODULE
## DESCRIPTION

## DEPENDENCIES

-   [Livekit](https://livekit.io/) # Media server (as webrtc server)
-   [PixiJs](https://pixijs.com/) # Webgl / Canvas renderer (as video manipulator)
-   [ExpressJs](https://expressjs.com/) # Node js server
-   [Svelte](https://svelte.dev/) ([vite](https://vitejs.dev/)) # Frontend framework
-   [WindiCss](https://windicss.org/) # CSS frameword
-   [RemixIcons](https://remixicon.com/) # Icon

## INSTRUCTIONS

### Preparation

#### Install livekit server

-   linux

```bash
curl -sSL https://get.livekit.io | bash
```

-   mac

```bash
brew install livekit
```

-   windows

```bash
# livekit-server.exe is included with this project
```

#### Install dependencies

-   Yarn

```bash
yarn
```

-   Npm

```bash
npm i
```

### Starting

#### Dev Mode

-   Run livekit server

```bash
livekit-server --dev
```

-   Run express server

```bash
yarn dev # or npm run dev
```

### Usage

#### API process

-   Create room
    -   room : is the room name / can use room id from database
    -   quota : is the participants number limit

```bash
curl --location --request POST 'http://localhost:3000/livekit/rooms' \
--header 'Content-Type: application/json' \
--data-raw '{
    "room":"testt",
    "quota":30
}'
```

-   List rooms

```bash
curl --location --request GET 'http://localhost:3000/livekit/rooms'
```

-   Delete room

```bash
curl --location --request DELETE 'http://localhost:3000/livekit/rooms/testt'
```

-   Create token

    token will be used for user can enter the room

    -   room : is the room name
    -   username : can be username or id from your database
    -   name : is the user's name
    -   publisher : set this to true for publisher / teacher account

```bash
curl --location --request POST 'http://localhost:3000/livekit/tokens' \
--header 'Content-Type: application/json' \
--data-raw '{
    "room":"testt",
    "username":"itsme",
    "name":"WPU",
    "publisher":false
}'
```

#### Client process

Publisher visit http://localhost:5173?token=TOKEN_FROM_CREATE_TOKEN_API_PUBLISHER

Click start session


Viewer visit http://localhost:5173?token=TOKEN_FROM_CREATE_TOKEN_API_VIEWER

Click connect
