import express from "express"
import { RoomServiceClient, Room, AccessToken } from 'livekit-server-sdk';
import * as dotenv from 'dotenv'
dotenv.config()
const router = express.Router()

const livekitHost = process.env.LIVE_URL;
const svc = new RoomServiceClient(livekitHost, process.env.LIVE_API, process.env.LIVE_SECRET);

router.get('/', (req, res) => {
    res.send('Birds home page')
})

router.post('/tokens', (req, res) => {
    const { room, publisher, userid, username, name } = req.body
    if (!room) return res.send("failed")
    if (!username) return res.send("failed")
    try {
        const at = new AccessToken(process.env.LIVE_API, process.env.LIVE_SECRET, {
            identity: username,
            name: name,
            metadata: JSON.stringify({ publisher: publisher, userid: userid }),
        });
        at.addGrant({
            roomJoin: true,
            room: room,
            canPublish: true,
            canSubscribe: true,
        });
        const token = at.toJwt();
        res.send(token)
    } catch (error) {
        res.send(error)
    }
})
router.get('/rooms', (req, res) => {
    svc.listRooms().then((rooms) => {
        res.send(rooms)
    });
})
router.post('/rooms', (req, res) => {
    try {
        const { room, timeout, quota, title, start, end } = req.body
        if (!room) return res.send("failed")
        const opts = {
            name: room,
            emptyTimeout: timeout || 10 * 60,
            maxParticipants: quota || 10,
            metadata: JSON.stringify({
                title,
                start,
                end
            })
        };
        svc.createRoom(opts).then((rooms) => {
            res.send(rooms)
        });
    } catch (error) {
        res.send(error)
    }
})
router.delete('/rooms/:room', (req, res) => {
    try {
        const { room } = req.params
        if (!room) return res.send("failed")
        svc.deleteRoom(room).then(() => {
            res.send('room deleted')
        });
    } catch (error) {
        res.send(error)
    }
})

export default router