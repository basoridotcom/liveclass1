import express from "express"
import cors from "cors"
import path from "path"
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import livekit from "./livekit.js"
import bodyParser from 'body-parser'
import * as dotenv from 'dotenv'
dotenv.config()


const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const app = express()
const port = process.env.SERVER_PORT || 5000

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.send('Hello World asasd!')
})

app.use("/livekit", livekit)


// app.use(express.static('dist'))
// app.get('*', (req, res) => {
//     res.sendFile(path.resolve(__dirname, 'dist', 'index.html'));
// })

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})